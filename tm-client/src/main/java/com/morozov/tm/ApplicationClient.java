package com.morozov.tm;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.service.Bootstrap;
import org.reflections.Reflections;

import java.util.Set;

public class ApplicationClient {
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("com.morozov.tm").getSubTypesOf(AbstractCommand.class);
    public static void main(String[] args) {
        new Bootstrap().init(classes);
    }
}
