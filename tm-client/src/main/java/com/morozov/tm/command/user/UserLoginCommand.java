package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractCommand {
    @NotNull
    @Override
    final public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "User authorisation";
    }

    @Override
    final public void execute() throws UserNotFoundException_Exception, StringEmptyException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        ConsoleHelperUtil.writeString("Введите логин");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль");
        @NotNull final String password = ConsoleHelperUtil.readString();
      @Nullable final Session session = serviceLocator.getSessionEndpoint().openSession(login, password);
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        serviceLocator.setSession(session);
        ConsoleHelperUtil.writeString("Вы вошли под учетной записью " + login);
    }
}
