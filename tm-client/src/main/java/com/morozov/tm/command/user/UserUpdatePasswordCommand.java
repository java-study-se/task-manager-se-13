package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdatePasswordCommand extends AbstractCommand {

    public UserUpdatePasswordCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-password-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update user password";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, UserNotFoundException_Exception,
            AccessFirbidenException_Exception, StringEmptyException_Exception, ConnectionLostException_Exception,
            SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите новый пароль");
        @NotNull final String newPassword = ConsoleHelperUtil.readString();
        serviceLocator.getUserEndpoint().updateUserPassword(session, newPassword);
        ConsoleHelperUtil.writeString("Пароль пользователя изменен");
    }
}

