package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected task";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception,
            StringEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите порядковый номер задачи для удаления");
        @NotNull final String idDeletedTask = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        if (serviceLocator.getTaskEndpoint().removeTaskById(session, idDeletedTask)) {
            ConsoleHelperUtil.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
        }
        ConsoleHelperUtil.writeString("Задачи с данным ID не существует");
    }
}
