package com.morozov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlJacksonSaveCommand extends AbstractCommand {
    public DataXmlJacksonSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data to xml file with Jax-B library";
    }

    @Override
    public void execute() throws IOException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Сохраняем в xml файл с помощью Jax-B");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().export(session, domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        Files.createDirectory(pathToFile.getParent());
        @NotNull final File sourceFile = pathToFile.toFile();
        @Nullable final ObjectMapper objectMapper = new XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_1_1, true);
        if (objectMapper == null) return;
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(sourceFile, domain);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
