package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectRemoveCommand extends AbstractCommand {
    public ProjectRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected project";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, StringEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID проекта для удаления");
        @NotNull final String idDeletedProject = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        if (serviceLocator.getProjectEndpoint().removeProjectById(session, idDeletedProject)) {
            ConsoleHelperUtil.writeString("Проект с ID: " + idDeletedProject + " удален");
            ConsoleHelperUtil.writeString("Удаление задач с ID проекта: " + idDeletedProject);
            serviceLocator.getTaskEndpoint().removeAllTaskByProjectId(session, idDeletedProject);
        } else {
            ConsoleHelperUtil.writeString("Проекта с данным ID не существует");
        }
    }
}
