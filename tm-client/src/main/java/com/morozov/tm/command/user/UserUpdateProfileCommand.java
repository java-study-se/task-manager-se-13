package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserUpdateProfileCommand extends AbstractCommand {

    public UserUpdateProfileCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "user-update-profile";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update user profile";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, UserNotFoundException_Exception,
            AccessFirbidenException_Exception, UserExistException_Exception, StringEmptyException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите новое имя пользователя");
        @NotNull final String newUserName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новый пароль");
        @NotNull final String newUserPassword = ConsoleHelperUtil.readString();
        serviceLocator.getUserEndpoint().updateUserProfile(session, newUserName, newUserPassword);
        ConsoleHelperUtil.writeString("Профиль обновлен");
    }
}
