package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.CompareTypeUnum;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ComparatorFactoryUtil;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all tasks";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception,
            RepositoryEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllTaskByUserId(session);
        ConsoleHelperUtil.writeString("Список задач пользователя");
        @NotNull final CompareTypeUnum compareTypeUnum = ConsoleHelperUtil.printSortedVariant();
        if (compareTypeUnum != null) {
            ConsoleHelperUtil.writeString("Сортировка по " + compareTypeUnum.getName());
            @NotNull final Task[] tasks = taskList.toArray(new Task[0]);
            Arrays.sort(tasks, ComparatorFactoryUtil.getComparator(compareTypeUnum));
            ConsoleHelperUtil.printTaskList(Arrays.asList(tasks));
        } else {
            ConsoleHelperUtil.writeString("Сортировка не выбрана.");
            ConsoleHelperUtil.printTaskList(taskList);
        }
    }
}
