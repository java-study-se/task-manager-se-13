package com.morozov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlJacksonLoadCommand extends AbstractCommand {
    public DataXmlJacksonLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-xml-jackson";
    }

    @Override
    public String getDescription() {
        return "Load data from xml file with Jax-B library";
    }

    @Override
    public void execute() throws IOException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_XML.getPath());
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        @NotNull final File sourceFile = pathToFile.toFile();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(sourceFile, Domain.class);
        serviceLocator.getDomainEndpoint().load(session, domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
