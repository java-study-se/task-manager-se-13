package com.morozov.tm.command.data.xml;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlJaxBSaveCommand extends AbstractCommand {
    public DataXmlJaxBSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Save data to xml file with Jax-B library";
    }

    @Override
    public void execute() throws IOException, JAXBException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Сохраняем в xml файл с помощью Jax-B");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().export(session, domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        Files.createDirectory(pathToFile.getParent());
        @NotNull final File sourceFile = pathToFile.toFile();
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        if (jaxbContext == null) return;
        @Nullable final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        if (jaxbMarshaller == null) return;
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(domain, sourceFile);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
