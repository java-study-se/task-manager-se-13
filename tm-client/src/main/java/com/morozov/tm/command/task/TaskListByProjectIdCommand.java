package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.CompareTypeUnum;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ComparatorFactoryUtil;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class TaskListByProjectIdCommand extends AbstractCommand {

    public TaskListByProjectIdCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-list-id";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all task by selected project";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, StringEmptyException_Exception,
            RepositoryEmptyException_Exception, AccessFirbidenException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Введите ID проекта");
        @NotNull final String projectId = ConsoleHelperUtil.readString();
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().findAllTaskByProjectId(session, projectId);
        ConsoleHelperUtil.writeString("Список задач по проекту с ID: " + projectId);
        @NotNull final CompareTypeUnum compareTypeUnum = ConsoleHelperUtil.printSortedVariant();
        if (compareTypeUnum != null) {
            ConsoleHelperUtil.writeString("Сортировка по " + compareTypeUnum.getName());
            @NotNull final Task[] tasks = taskList.toArray(new Task[0]);
            Arrays.sort(tasks, ComparatorFactoryUtil.getComparator(compareTypeUnum));
            ConsoleHelperUtil.printTaskList(Arrays.asList(tasks));
        } else {
            ConsoleHelperUtil.writeString("Сортировка не выбрана.");
            ConsoleHelperUtil.printTaskList(taskList);
        }
    }
}
