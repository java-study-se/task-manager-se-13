package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.ConnectionLostException_Exception;
import com.morozov.tm.endpoint.SqlCustomException_Exception;
import com.morozov.tm.endpoint.StringEmptyException_Exception;
import com.morozov.tm.endpoint.UserExistException_Exception;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

public class UserRegistryCommand extends AbstractCommand {
    @NotNull
    @Override
    final public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Registry new user";
    }

    @Override
    final public void execute() throws UserExistException_Exception, StringEmptyException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        ConsoleHelperUtil.writeString("Введите имя пользователя");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль пользователя");
        @NotNull final String password = ConsoleHelperUtil.readString();
        serviceLocator.getUserEndpoint().registryUser(login,password);
        ConsoleHelperUtil.writeString("Пользователь с логином " + login + " зарегистрирован");
    }
}
