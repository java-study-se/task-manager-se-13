package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Create project";
    }

    @Override
    final public void execute() throws AccessFirbidenException_Exception, StringEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        ConsoleHelperUtil.writeString("Введите имя нового проекта");
        @NotNull final String projectName = ConsoleHelperUtil.readString();
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
            @NotNull final Project addedProject = serviceLocator.getProjectEndpoint().addProject(session,projectName);
            ConsoleHelperUtil.writeString("Добавлен проект: " + addedProject.getName());
    }
}
