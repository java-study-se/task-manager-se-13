package com.morozov.tm.command.data.xml;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataXmlJaxBLoadCommand extends AbstractCommand {
    public DataXmlJaxBLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-xml-jaxb";
    }

    @Override
    public String getDescription() {
        return "Load data from xml file with Jax-B library";
    }

    @Override
    public void execute() throws JAXBException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_XML.getPath());
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_XML.getPath());
        @NotNull final File sourceFile = pathToFile.toFile();
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        if (jaxbContext == null) return;
        @Nullable final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        if (jaxbUnmarshaller == null) return;
        @Nullable final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(sourceFile);
        serviceLocator.getDomainEndpoint().load(session,domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
