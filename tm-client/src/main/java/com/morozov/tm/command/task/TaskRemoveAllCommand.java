package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveAllCommand extends AbstractCommand {
    public TaskRemoveAllCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @Override
    public String getName() {
        return "task-removeall";
    }

    @Override
    public String getDescription() {
        return "Remove all task current user";
    }

    @Override
    public void execute() throws CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Удаление задач пользователя");
        serviceLocator.getTaskEndpoint().removeAllTaskByUserId(session);
        ConsoleHelperUtil.writeString("Удаление задач завершено");
    }
}
