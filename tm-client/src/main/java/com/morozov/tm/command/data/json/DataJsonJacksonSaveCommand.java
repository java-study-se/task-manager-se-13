package com.morozov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataJsonJacksonSaveCommand extends AbstractCommand {
    public DataJsonJacksonSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-json-jackson";
    }

    @Override
    public String getDescription() {
        return "Save data to json file with Jackson library";
    }

    @Override
    public void execute() throws IOException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Сохраняем в json файл с помощью Jax-B");
        @Nullable final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().export(session, domain);
        @NotNull final Path pathToFile = Paths.get(DataConstant.DATA_FILE_JSON.getPath());
        @Nullable final File sourceFile = pathToFile.toFile();
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        if (objectWriter == null) return;
        objectWriter.writeValue(sourceFile, domain);
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
