package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractCommand {
    public ProjectClearCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove all projects";
    }

    @Override
    final public void execute() throws CloneNotSupportedException_Exception, AccessFirbidenException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        serviceLocator.getTaskEndpoint().clearTaskList(session);
        ConsoleHelperUtil.writeString("Список задач очищен");
        serviceLocator.getProjectEndpoint().clearProjectList(session);
        ConsoleHelperUtil.writeString("Список проектов очищен");
    }
}
