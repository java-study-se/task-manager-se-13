package com.morozov.tm.command.data.bin;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataBinLoadCommand extends AbstractCommand {
    public DataBinLoadCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "dataload-bin";
    }

    @Override
    public String getDescription() {
        return "Load data from binnary file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException, CloneNotSupportedException_Exception,
            AccessFirbidenException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if(session == null){
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Читаем из файла " + DataConstant.DATA_FILE_BIN.getPath());
        @Nullable final Path pathToFile = Paths.get(DataConstant.DATA_FILE_BIN.getPath());
        @Nullable File sourceFile;
        if (pathToFile == null) return;
        sourceFile = pathToFile.toFile();
        @Nullable FileInputStream fileInputStream;
        if (sourceFile == null) return;
        fileInputStream = new FileInputStream(sourceFile);
        @Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        if (domain == null) return;
        serviceLocator.getDomainEndpoint().load(session,domain);
        ConsoleHelperUtil.writeString("Файл " + sourceFile.getPath() + " загружен");
    }
}
