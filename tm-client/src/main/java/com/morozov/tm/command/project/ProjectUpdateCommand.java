package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectUpdateCommand extends AbstractCommand {
    public ProjectUpdateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Update selected project";
    }

    @Override
    final public void execute() throws ProjectNotFoundException_Exception, RepositoryEmptyException_Exception,
            CloneNotSupportedException_Exception, ParseException_Exception, AccessFirbidenException_Exception,
            StringEmptyException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите ID проекта для изменения");
        @NotNull final String updateProjectId = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое имя проекта");
        @NotNull final String updateProjectName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новое описание проекта");
        @NotNull final String updateProjectDescription = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату начала проекта в формате DD.MM.YYYY");
        @NotNull final String startUpdateDate = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите новую дату окончания проекта в формате DD.MM.YYYY");
        @NotNull final String endUpdateDate = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        serviceLocator.getProjectEndpoint().updateProject(session, updateProjectId, updateProjectName, updateProjectDescription,
                startUpdateDate, endUpdateDate);
        ConsoleHelperUtil.writeString("Проект изменен");
    }
}
