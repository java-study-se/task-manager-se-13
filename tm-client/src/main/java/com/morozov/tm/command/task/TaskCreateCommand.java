package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Create task";
    }

    @Override
    final public void execute()
            throws AccessFirbidenException_Exception, StringEmptyException_Exception, CloneNotSupportedException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        ConsoleHelperUtil.writeString("Введите имя новой задачи");
        @NotNull final String taskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String projectId = ConsoleHelperUtil.readString();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        @NotNull final Task task = serviceLocator.getTaskEndpoint().addTask(session, taskName, projectId);
        ConsoleHelperUtil.writeString("Добавлена задача " + task.getName());
    }
}
