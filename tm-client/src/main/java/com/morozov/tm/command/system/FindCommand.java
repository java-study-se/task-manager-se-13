package com.morozov.tm.command.system;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class FindCommand extends AbstractCommand {
    public FindCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @Override
    public String getName() {
        return "find";
    }

    @Override
    public String getDescription() {
        return "Find project and task by name or description";
    }

    @Override
    public void execute() throws AccessFirbidenException_Exception,
            RepositoryEmptyException_Exception, CloneNotSupportedException_Exception, ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if (session == null) {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Введите строку для поиска");
        @NotNull final String findString = ConsoleHelperUtil.readString().trim();
        @NotNull final List<Project> projectsList = serviceLocator.getProjectEndpoint()
                .findProjectByStringInNameOrDescription(session, findString);
        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint()
                .findTaskByStringInNameOrDescription(session, findString);
        if (findString.isEmpty()) {
            ConsoleHelperUtil.writeString("Введенная строка пуста, будет выведен полный список проектов и задач");
        }
        if(!projectsList.isEmpty()) ConsoleHelperUtil.printProjectList(projectsList);
        else ConsoleHelperUtil.writeString("Проекты по строке поиска: " + findString + " не найдены");
        if(!taskList.isEmpty()) ConsoleHelperUtil.printTaskList(taskList);
        else ConsoleHelperUtil.writeString("Задачи по строке поиска: " + findString + " не найдены");
    }
}
