package com.morozov.tm.command.data.bin;


import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.endpoint.*;
import com.morozov.tm.enumerated.DataConstant;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DataBinSaveCommand extends AbstractCommand {
    public DataBinSaveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "datasave-bin";
    }

    @Override
    public String getDescription() {
        return "Save data to binary file";
    }

    @Override
    public void execute() throws IOException, CloneNotSupportedException_Exception, AccessFirbidenException_Exception,
            ConnectionLostException_Exception, SqlCustomException_Exception {
        @Nullable final Session session = serviceLocator.getSession();
        if(session == null){
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
            return;
        }
        ConsoleHelperUtil.writeString("Сохраняем в bin файл");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().export(session,domain);
        @Nullable final Path pathToFile = Paths.get(DataConstant.DATA_FILE_BIN.getPath());
        if (pathToFile == null) return;
        Files.createDirectory(pathToFile.getParent());
        @Nullable File sourceFile;
        sourceFile = pathToFile.toFile();
        @Nullable FileOutputStream fileOutputStream;
        if (sourceFile == null) return;
        fileOutputStream = new FileOutputStream(sourceFile);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        ConsoleHelperUtil.writeString("Данные сохранены в файл " + sourceFile.getPath());
    }
}
