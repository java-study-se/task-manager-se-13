
package com.morozov.tm.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-19T12:02:30.982+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "ProjectNotFoundException", targetNamespace = "http://endpoint.tm.morozov.com/")
public class ProjectNotFoundException_Exception extends Exception {

    private com.morozov.tm.endpoint.ProjectNotFoundException projectNotFoundException;

    public ProjectNotFoundException_Exception() {
        super();
    }

    public ProjectNotFoundException_Exception(String message) {
        super(message);
    }

    public ProjectNotFoundException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public ProjectNotFoundException_Exception(String message, com.morozov.tm.endpoint.ProjectNotFoundException projectNotFoundException) {
        super(message);
        this.projectNotFoundException = projectNotFoundException;
    }

    public ProjectNotFoundException_Exception(String message, com.morozov.tm.endpoint.ProjectNotFoundException projectNotFoundException, java.lang.Throwable cause) {
        super(message, cause);
        this.projectNotFoundException = projectNotFoundException;
    }

    public com.morozov.tm.endpoint.ProjectNotFoundException getFaultInfo() {
        return this.projectNotFoundException;
    }
}
