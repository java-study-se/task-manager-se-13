package com.morozov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-19T12:02:30.993+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.morozov.com/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectByIdResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectById/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectById/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectById/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectById/Fault/CloneNotSupportedException"), @FaultAction(className = StringEmptyException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeProjectById/Fault/StringEmptyException")})
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.RemoveProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeProjectById(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception, StringEmptyException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserIdRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserIdResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserId/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserId/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserId/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserId/Fault/CloneNotSupportedException"), @FaultAction(className = RepositoryEmptyException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectByUserId/Fault/RepositoryEmptyException")})
    @RequestWrapper(localName = "findAllProjectByUserId", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindAllProjectByUserId")
    @ResponseWrapper(localName = "findAllProjectByUserIdResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindAllProjectByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.morozov.tm.endpoint.Project> findAllProjectByUserId(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception, RepositoryEmptyException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProjectResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProject/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProject/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProject/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findAllProject/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "findAllProject", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindAllProject")
    @ResponseWrapper(localName = "findAllProjectResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindAllProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.morozov.tm.endpoint.Project> findAllProject(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserIdRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserIdResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserId/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserId/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserId/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/removeAllProjectByUserId/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "removeAllProjectByUserId", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.RemoveAllProjectByUserId")
    @ResponseWrapper(localName = "removeAllProjectByUserIdResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.RemoveAllProjectByUserIdResponse")
    public void removeAllProjectByUserId(
        @WebParam(name = "userId", targetNamespace = "")
        com.morozov.tm.endpoint.Session userId
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectListRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectListResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectList/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectList/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectList/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/clearProjectList/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "clearProjectList", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.ClearProjectList")
    @ResponseWrapper(localName = "clearProjectListResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.ClearProjectListResponse")
    public void clearProjectList(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescriptionRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescriptionResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescription/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescription/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescription/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/findProjectByStringInNameOrDescription/Fault/CloneNotSupportedException")})
    @RequestWrapper(localName = "findProjectByStringInNameOrDescription", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindProjectByStringInNameOrDescription")
    @ResponseWrapper(localName = "findProjectByStringInNameOrDescriptionResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.FindProjectByStringInNameOrDescriptionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<com.morozov.tm.endpoint.Project> findProjectByStringInNameOrDescription(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session,
        @WebParam(name = "string", targetNamespace = "")
        java.lang.String string
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProjectRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProjectResponse", fault = {@FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProject/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProject/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProject/Fault/SqlCustomException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProject/Fault/CloneNotSupportedException"), @FaultAction(className = StringEmptyException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/addProject/Fault/StringEmptyException")})
    @RequestWrapper(localName = "addProject", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.AddProject")
    @ResponseWrapper(localName = "addProjectResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.AddProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.morozov.tm.endpoint.Project addProject(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName
    ) throws ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, CloneNotSupportedException_Exception, StringEmptyException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProjectRequest", output = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProjectResponse", fault = {@FaultAction(className = ParseException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/ParseException"), @FaultAction(className = CloneNotSupportedException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/CloneNotSupportedException"), @FaultAction(className = ProjectNotFoundException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/ProjectNotFoundException"), @FaultAction(className = ConnectionLostException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/ConnectionLostException"), @FaultAction(className = AccessFirbidenException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/AccessFirbidenException"), @FaultAction(className = SqlCustomException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/SqlCustomException"), @FaultAction(className = StringEmptyException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/StringEmptyException"), @FaultAction(className = RepositoryEmptyException_Exception.class, value = "http://endpoint.tm.morozov.com/ProjectEndpoint/updateProject/Fault/RepositoryEmptyException")})
    @RequestWrapper(localName = "updateProject", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.UpdateProject")
    @ResponseWrapper(localName = "updateProjectResponse", targetNamespace = "http://endpoint.tm.morozov.com/", className = "com.morozov.tm.endpoint.UpdateProjectResponse")
    public void updateProject(
        @WebParam(name = "session", targetNamespace = "")
        com.morozov.tm.endpoint.Session session,
        @WebParam(name = "projectId", targetNamespace = "")
        java.lang.String projectId,
        @WebParam(name = "projectName", targetNamespace = "")
        java.lang.String projectName,
        @WebParam(name = "projectDescription", targetNamespace = "")
        java.lang.String projectDescription,
        @WebParam(name = "dataStartProject", targetNamespace = "")
        java.lang.String dataStartProject,
        @WebParam(name = "dataEndProject", targetNamespace = "")
        java.lang.String dataEndProject
    ) throws ParseException_Exception, CloneNotSupportedException_Exception, ProjectNotFoundException_Exception, ConnectionLostException_Exception, AccessFirbidenException_Exception, SqlCustomException_Exception, StringEmptyException_Exception, RepositoryEmptyException_Exception;
}
