
package com.morozov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.morozov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConnectionLostException_QNAME = new QName("http://endpoint.tm.morozov.com/", "ConnectionLostException");
    private final static QName _SqlCustomException_QNAME = new QName("http://endpoint.tm.morozov.com/", "SqlCustomException");
    private final static QName _StringEmptyException_QNAME = new QName("http://endpoint.tm.morozov.com/", "StringEmptyException");
    private final static QName _UserNotFoundException_QNAME = new QName("http://endpoint.tm.morozov.com/", "UserNotFoundException");
    private final static QName _CloseSession_QNAME = new QName("http://endpoint.tm.morozov.com/", "closeSession");
    private final static QName _CloseSessionResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "closeSessionResponse");
    private final static QName _OpenSession_QNAME = new QName("http://endpoint.tm.morozov.com/", "openSession");
    private final static QName _OpenSessionResponse_QNAME = new QName("http://endpoint.tm.morozov.com/", "openSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.morozov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConnectionLostException }
     * 
     */
    public ConnectionLostException createConnectionLostException() {
        return new ConnectionLostException();
    }

    /**
     * Create an instance of {@link SqlCustomException }
     * 
     */
    public SqlCustomException createSqlCustomException() {
        return new SqlCustomException();
    }

    /**
     * Create an instance of {@link StringEmptyException }
     * 
     */
    public StringEmptyException createStringEmptyException() {
        return new StringEmptyException();
    }

    /**
     * Create an instance of {@link UserNotFoundException }
     * 
     */
    public UserNotFoundException createUserNotFoundException() {
        return new UserNotFoundException();
    }

    /**
     * Create an instance of {@link CloseSession }
     * 
     */
    public CloseSession createCloseSession() {
        return new CloseSession();
    }

    /**
     * Create an instance of {@link CloseSessionResponse }
     * 
     */
    public CloseSessionResponse createCloseSessionResponse() {
        return new CloseSessionResponse();
    }

    /**
     * Create an instance of {@link OpenSession }
     * 
     */
    public OpenSession createOpenSession() {
        return new OpenSession();
    }

    /**
     * Create an instance of {@link OpenSessionResponse }
     * 
     */
    public OpenSessionResponse createOpenSessionResponse() {
        return new OpenSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConnectionLostException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "ConnectionLostException")
    public JAXBElement<ConnectionLostException> createConnectionLostException(ConnectionLostException value) {
        return new JAXBElement<ConnectionLostException>(_ConnectionLostException_QNAME, ConnectionLostException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SqlCustomException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "SqlCustomException")
    public JAXBElement<SqlCustomException> createSqlCustomException(SqlCustomException value) {
        return new JAXBElement<SqlCustomException>(_SqlCustomException_QNAME, SqlCustomException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StringEmptyException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "StringEmptyException")
    public JAXBElement<StringEmptyException> createStringEmptyException(StringEmptyException value) {
        return new JAXBElement<StringEmptyException>(_StringEmptyException_QNAME, StringEmptyException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserNotFoundException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "UserNotFoundException")
    public JAXBElement<UserNotFoundException> createUserNotFoundException(UserNotFoundException value) {
        return new JAXBElement<UserNotFoundException>(_UserNotFoundException_QNAME, UserNotFoundException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "closeSession")
    public JAXBElement<CloseSession> createCloseSession(CloseSession value) {
        return new JAXBElement<CloseSession>(_CloseSession_QNAME, CloseSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloseSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "closeSessionResponse")
    public JAXBElement<CloseSessionResponse> createCloseSessionResponse(CloseSessionResponse value) {
        return new JAXBElement<CloseSessionResponse>(_CloseSessionResponse_QNAME, CloseSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "openSession")
    public JAXBElement<OpenSession> createOpenSession(OpenSession value) {
        return new JAXBElement<OpenSession>(_OpenSession_QNAME, OpenSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.morozov.com/", name = "openSessionResponse")
    public JAXBElement<OpenSessionResponse> createOpenSessionResponse(OpenSessionResponse value) {
        return new JAXBElement<OpenSessionResponse>(_OpenSessionResponse_QNAME, OpenSessionResponse.class, null, value);
    }

}
