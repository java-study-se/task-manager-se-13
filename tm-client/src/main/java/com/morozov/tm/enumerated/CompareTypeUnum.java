package com.morozov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum CompareTypeUnum {
    DATACREATE("data-create", "Sorted by create data"),
    DATASTART("data-start", "Sorted by start data"),
    DATAEND("data-end", "Sorted by end data"),
    STATUS("status", "Sorted by status");
    private String name;
    private String description;

    CompareTypeUnum(@NotNull final String name,@NotNull final String description) {
        this.name = name;
        this.description = description;
    }
    @NotNull
    public String getName() {
        return name;
    }
    @NotNull
    public String getDescription() {
        return description;
    }
}
