package com.morozov.tm.endpoint;

import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class UserEndpoint {

    public UserEndpoint(Bootstrap bootstrap) {
        this.userService = bootstrap.getUserService();
        this.sessionService = bootstrap.getSessionService();
    }

    private IUserService userService;
    private ISessionService sessionService;

    @WebMethod
    public void registryUser(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password)
            throws UserExistException, StringEmptyException, ConnectionLostException, SqlCustomException {
        userService.registryUser(login, password);
    }

    @WebMethod
    public void updateUserPassword(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "newPassword") String newPassword)
            throws StringEmptyException, UserNotFoundException, CloneNotSupportedException, AccessFirbidenException,
            ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) return;
        userService.updateUserPassword(currentSession.getUserId(), newPassword);
    }

    @WebMethod
    public void updateUserProfile(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "newUserName") String newUserName,
            @NotNull @WebParam(name = "newUserPassword") String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, CloneNotSupportedException,
            AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) return;
        userService.updateUserProfile(currentSession.getUserId(), newUserName, newUserPassword);
    }

    @WebMethod
    public User findOneUserById(
            @WebParam(name = "session") Session session)
            throws UserNotFoundException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return userService.findOneById(currentSession.getUserId());
    }
}
