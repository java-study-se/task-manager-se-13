package com.morozov.tm.endpoint;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.entity.Session;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class DomainEndpoint {
    private IServiceLocator serviceLocator;
    private ISessionService sessionService;

    public DomainEndpoint(Bootstrap bootstrap) {
        this.serviceLocator = bootstrap;
        this.sessionService = bootstrap.getSessionService();
    }

    @WebMethod
    public void load(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "domain") final Domain domain)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        sessionService.validate(session);
        serviceLocator.getDomainService().load(domain);

    }

    @WebMethod
    public void export(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "domain") final Domain domain)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        sessionService.validate(session);
        serviceLocator.getDomainService().export(domain);
    }
}
