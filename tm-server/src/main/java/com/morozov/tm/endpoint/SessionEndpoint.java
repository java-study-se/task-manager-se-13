package com.morozov.tm.endpoint;

import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.service.Bootstrap;
import com.morozov.tm.util.ConsoleHelperUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
@NoArgsConstructor
public class SessionEndpoint {
    private IUserService userService;
    private ISessionService sessionService;

    public SessionEndpoint(Bootstrap bootstrap) {
        this.userService = bootstrap.getUserService();
        this.sessionService = bootstrap.getSessionService();
    }

    @WebMethod
    @Nullable
    public Session openSession(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password)
            throws UserNotFoundException, StringEmptyException, ConnectionLostException, SqlCustomException {
        final User currentUser = userService.loginUser(login, password);
        @NotNull final Session newSession = sessionService.getNewSession(currentUser);
        System.out.println(String.format("Выдана сессия: \n Имя пользователя: %s \n ID сессии: %s \n ID клиента: %s \n",
                currentUser.getLogin(), newSession.getId(), newSession.getUserId()));
        return newSession;
    }

    @WebMethod
    public void closeSession(
            @WebParam(name = "session") Session session)
            throws ConnectionLostException, SqlCustomException {
        sessionService.closeSession(session);
        ConsoleHelperUtil.writeString("Закрыта сессия ID " + session.getId());
    }
}
