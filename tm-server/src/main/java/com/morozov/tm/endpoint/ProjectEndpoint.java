package com.morozov.tm.endpoint;

import com.morozov.tm.api.IProjectService;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Session;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndpoint {
    private IProjectService projectService;
    private ISessionService sessionService;

    public ProjectEndpoint(Bootstrap bootstrap) {
        this.projectService = bootstrap.getProjectService();
        this.sessionService = bootstrap.getSessionService();
    }

    public List<Project> findAllProject(
            @NotNull @WebParam(name = "session") Session session)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        sessionService.validate(session);
        return projectService.findAllProject();
    }

    @WebMethod
    public List<Project> findAllProjectByUserId(
            @WebParam(name = "session") Session session)
            throws RepositoryEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return projectService.getAllProjectByUserId(currentSession.getUserId());
    }

    @WebMethod
    public Project addProject(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectName") String projectName)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return projectService.addProject(currentSession.getUserId(), projectName);
    }

    @WebMethod
    public boolean removeProjectById(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String id)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return projectService.removeProjectById(currentSession.getUserId(), id);
    }

    @WebMethod
    public void updateProject(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String id,
            @NotNull @WebParam(name = "projectName") String projectName,
            @NotNull @WebParam(name = "projectDescription") String projectDescription,
            @NotNull @WebParam(name = "dataStartProject") String dataStart,
            @NotNull @WebParam(name = "dataEndProject") String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException, ProjectNotFoundException,
            CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        projectService.updateProject(currentSession.getUserId(), id, projectName, projectDescription, dataStart, dataEnd);
    }

    @WebMethod
    public List<Project> findProjectByStringInNameOrDescription(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "string") String string)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return projectService.findProjectByStringInNameOrDescription(currentSession.getUserId(), string);
    }

    @WebMethod
    public void removeAllProjectByUserId(
            @WebParam(name = "userId") Session session)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        projectService.removeAllByUserId(currentSession.getUserId());
    }

    @WebMethod
    public void clearProjectList(
            @WebParam(name = "session") Session session)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        sessionService.validate(session);
        projectService.clearProjectList();
    }
}
