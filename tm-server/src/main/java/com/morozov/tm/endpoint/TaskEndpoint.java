package com.morozov.tm.endpoint;

import com.morozov.tm.api.ISessionService;
import com.morozov.tm.api.ITaskService;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.*;
import com.morozov.tm.service.Bootstrap;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskEndpoint {


    public TaskEndpoint(Bootstrap bootstrap) {
        this.taskService = bootstrap.getTaskService();
        this.sessionService = bootstrap.getSessionService();
    }

    private ITaskService taskService;
    private ISessionService sessionService;

    @WebMethod
    public List<Task> findAllTaskByUserId(
            @NotNull @WebParam(name = "session") Session session)
            throws RepositoryEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return taskService.getAllTaskByUserId(currentSession.getUserId());
    }

    @WebMethod
    public Task addTask(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "taskName") String taskName,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return taskService.addTask(currentSession.getUserId(), taskName, projectId);
    }

    @WebMethod
    public boolean removeTaskById(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "taskId") String taskId)
            throws StringEmptyException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return taskService.removeTaskById(currentSession.getUserId(), taskId);
    }

    @WebMethod
    public void updateTask(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "taskId") String taskId,
            @NotNull @WebParam(name = "taskName") String taskName,
            @NotNull @WebParam(name = "taskDescription") String taskDescription,
            @NotNull @WebParam(name = "dataStartTask") String dataStart,
            @NotNull @WebParam(name = "dataEndTask") String dataEnd,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException,
            TaskNotFoundException, CloneNotSupportedException, AccessFirbidenException, ConnectionLostException,
            SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        taskService.updateTask(currentSession.getUserId(), taskId, taskName, taskDescription,
                dataStart, dataEnd, projectId);
    }

    @WebMethod
    public List<Task> findAllTaskByProjectId(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws StringEmptyException, RepositoryEmptyException, CloneNotSupportedException, AccessFirbidenException,
            ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return taskService.getAllTaskByProjectId(currentSession.getUserId(), projectId);
    }

    @WebMethod
    public void removeAllTaskByProjectId(
            @NotNull @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "projectId") String projectId)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        taskService.removeAllTaskByProjectId(currentSession.getUserId(), projectId);
    }

    @WebMethod
    public void clearTaskList(
            @NotNull @WebParam(name = "session") Session session)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        sessionService.validate(session);
        taskService.clearTaskList();
    }

    @WebMethod
    public List<Task> findTaskByStringInNameOrDescription(
            @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "string") String string)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        return taskService.findTaskByStringInNameOrDescription(currentSession.getUserId(), string);
    }

    @WebMethod
    public void removeAllTaskByUserId(
            @WebParam(name = "session") Session session)
            throws CloneNotSupportedException, AccessFirbidenException, ConnectionLostException, SqlCustomException {
        Session currentSession = sessionService.validate(session);
        if (currentSession.getUserId() == null) throw new AccessFirbidenException();
        taskService.removeAllTaskByUserId(currentSession.getUserId());
    }
}
