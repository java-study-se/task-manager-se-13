package com.morozov.tm.entity;

import com.morozov.tm.enumerated.UserRoleEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class Session extends AbstractEntity implements Cloneable {
    @Nullable
    private String userId = null;
    @Nullable
    private String signature = null;
    @Nullable
    private Date timestamp = null;
    @Nullable
    private UserRoleEnum userRoleEnum = null;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }
}
