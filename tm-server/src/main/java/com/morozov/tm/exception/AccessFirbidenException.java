package com.morozov.tm.exception;

public class AccessFirbidenException extends Exception {
    public AccessFirbidenException() {
        super("Доступ запрещен");
    }
}
