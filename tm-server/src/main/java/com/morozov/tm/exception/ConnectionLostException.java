package com.morozov.tm.exception;

public class ConnectionLostException extends Exception {
    public ConnectionLostException() {
        super("Отсутствует соединение с БД");
    }
}
