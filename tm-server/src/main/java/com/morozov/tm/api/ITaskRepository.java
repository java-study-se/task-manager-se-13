package com.morozov.tm.api;

import com.morozov.tm.entity.Task;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {
    @Select(
            "SELECT * FROM tm_morozov.app_task"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @NotNull
    List<Task> findAll() throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @Nullable
    Task findOne(@NotNull final String id) throws SQLException;

    @Insert(
            "INSERT INTO tm_morozov.app_task (id, date_create, date_end, date_start, description, name, status, user_id, project_id) " +
                    "VALUES (#{id},#{createdData},#{endDate},#{startDate},#{description},#{name},#{status},#{userId},#{idProject})"
    )
    void persist(@NotNull final Task writeEntity) throws SQLException;

    @Update(
            "UPDATE tm_morozov.app_task SET date_create = #{createdData}, date_end = #{endDate}, date_start = #{startDate}, " +
                    "name = #{name}, description = #{description}, status = #{status},user_id = #{userId}, project_id = #{idProject}" +
                    "WHERE id = #{id}"
    )
    void merge(@NotNull final Task updateEntity) throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_task WHERE id=#{id}"
    )
    void remove(@NotNull final String id) throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_task"
    )
    void removeAll() throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE project_id = #{projectId} AND user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @NotNull
    List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @NotNull
    List<Task> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE user_id = #{userId} AND id = #{id}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @Nullable
    Task findOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE project_id = #{project_id}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @NotNull
    List<Task> getAllTaskByProjectId(@NotNull final String projectId) throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_task WHERE project_id = #{projectId} AND user_id = #{userId}"
    )
    void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_task WHERE user_id = #{userId} AND " +
                    "(name LIKE CONCAT('%', #{string}, '%') OR description LIKE CONCAT('%', #{string}, '%'))"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "idProject", column = "project_id")
    })
    @NotNull
    List<Task> findTaskByStringInNameOrDescription(@Param(value = "userId")@NotNull final String userId,
                                                   @Param(value = "string")@NotNull final String string) throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_task WHERE user_id = #{userId}"
    )
    void removeAllByUserId(@NotNull final String userId) throws SQLException;
}
