package com.morozov.tm.api;

import com.morozov.tm.entity.User;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {
    @Select(
            "SELECT * FROM tm_morozov.app_user"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role")
    })
    @NotNull
    List<User> findAll()throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role")
    })
    @Nullable
    User findOne(@NotNull final String id) throws SQLException;

    @Insert(
            "INSERT INTO tm_morozov.app_user (id, login, password_hash, role) VALUES (#{id},#{login},#{passwordHash},#{role})"
    )
    void persist(@NotNull final User writeEntity)throws SQLException;

    @Update(
            "UPDATE tm_morozov.app_user SET login = #{login} , password_hash = #{passwordHash} , role = #{role} " +
                    "WHERE  id = #{id}"
    )
    void merge(@NotNull final User updateEntity)throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_user WHERE id=#{id}"
    )
    void remove(@NotNull final String id)throws SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_user"
    )
    void removeAll()throws SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_user WHERE login = #{login}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "role", column = "role")
    })
    @Nullable
    User findOneByLogin(@NotNull final String login)throws SQLException;
}

