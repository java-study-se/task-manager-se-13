package com.morozov.tm.api;

import com.morozov.tm.entity.Domain;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import org.jetbrains.annotations.NotNull;

public interface IDomainService {
    void load(@NotNull Domain domain) throws ConnectionLostException, SqlCustomException;
    void export(@NotNull Domain domain) throws ConnectionLostException, SqlCustomException;
}
