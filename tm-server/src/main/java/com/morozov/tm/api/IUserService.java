package com.morozov.tm.api;

import com.morozov.tm.entity.User;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IUserService {
    @NotNull
    User loginUser(@NotNull String login, @NotNull String password) throws UserNotFoundException, StringEmptyException, ConnectionLostException, SqlCustomException;

    @NotNull
    String registryUser(@NotNull String login, @NotNull String password) throws UserExistException, StringEmptyException, ConnectionLostException, SqlCustomException;

    void updateUserPassword(@NotNull String id, @NotNull String newPassword) throws StringEmptyException, UserNotFoundException, ConnectionLostException, SqlCustomException;

    void updateUserProfile(@NotNull String id, @NotNull String newUserName, @NotNull String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException, SqlCustomException;

    void loadUserList(@Nullable final List<User> userList) throws ConnectionLostException, SqlCustomException;

    @NotNull
    List<User> findAllUser() throws ConnectionLostException, SqlCustomException;

    @NotNull
    User findOneById(@NotNull final String id) throws UserNotFoundException, ConnectionLostException, SqlCustomException;
}
