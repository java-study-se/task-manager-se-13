package com.morozov.tm.api;

import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    @NotNull
    List<Task> findAllTask() throws ConnectionLostException, SqlCustomException;
    @NotNull
    List<Task> getAllTaskByUserId(@NotNull String userId) throws RepositoryEmptyException, ConnectionLostException, SqlCustomException;
    @NotNull
    Task addTask(@NotNull String userId, @NotNull String taskName, @NotNull String projectId)
            throws StringEmptyException, ConnectionLostException, SqlCustomException;

    boolean removeTaskById(@NotNull String userId, @NotNull String id) throws StringEmptyException, ConnectionLostException, SqlCustomException;

    void updateTask(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
                    @NotNull String dataStart, @NotNull String dataEnd, @NotNull String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException, TaskNotFoundException, ConnectionLostException, SqlCustomException;
    @NotNull
    List<Task> getAllTaskByProjectId(@NotNull String userId, @NotNull String projectId)
            throws StringEmptyException, RepositoryEmptyException, ConnectionLostException, SqlCustomException;

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws ConnectionLostException, SqlCustomException;

    void clearTaskList() throws ConnectionLostException, SqlCustomException;
    @NotNull
    List<Task> findTaskByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
            throws ConnectionLostException, SqlCustomException;

    void removeAllTaskByUserId(@NotNull String userId) throws ConnectionLostException, SqlCustomException;

    void loadTaskList(@Nullable final List<Task> loadList) throws ConnectionLostException, SqlCustomException;
}
