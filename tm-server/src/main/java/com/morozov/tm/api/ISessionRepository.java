package com.morozov.tm.api;

import com.morozov.tm.entity.Session;
import com.morozov.tm.exception.ConnectionLostException;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {
    @Select(
            "SELECT * FROM tm_morozov.app_session"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp")
    })
    @NotNull
    List<Session> findAll() throws ConnectionLostException, SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_session WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "timestamp", column = "timestamp")
    })
    @Nullable
    Session findOne(@NotNull final String id) throws SQLException, ConnectionLostException;

    @Insert(
            "INSERT INTO tm_morozov.app_session (id, signature, timestamp, user_id) " +
                    "VALUES (#{id},#{signature},#{timestamp},#{userId})"
    )
    void persist(@NotNull final Session writeEntity) throws ConnectionLostException, SQLException;

    @Update(
            "UPDATE tm_morozov.app_session SET signature = #{signature}, timestamp = #{timestamp}, user_id = #{userId} " +
                    "WHERE id = #{id}"
    )
    void merge(@NotNull final Session updateEntity) throws ConnectionLostException, SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_session WHERE id=#{id}"
    )
    void remove(@NotNull final String id) throws ConnectionLostException, SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_session"
    )
    void removeAll() throws ConnectionLostException, SQLException;
}
