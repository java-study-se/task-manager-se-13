package com.morozov.tm.api;

import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import org.jetbrains.annotations.NotNull;



public interface ISessionService {

    Session getNewSession(final User user) throws ConnectionLostException, SqlCustomException;

    @NotNull
    Session validate(final Session session) throws AccessFirbidenException, CloneNotSupportedException, ConnectionLostException, SqlCustomException;

    void closeSession(@NotNull final Session session) throws ConnectionLostException, SqlCustomException;
}
