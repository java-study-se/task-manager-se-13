package com.morozov.tm.api;


import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.ConnectionLostException;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {
    @Select(
            "SELECT * FROM tm_morozov.app_project"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<Project> findAll() throws ConnectionLostException, SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_project WHERE id = #{id}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Project findOne(@NotNull final String id) throws SQLException, ConnectionLostException;

    @Insert(
            "INSERT INTO tm_morozov.app_project (id, date_create, date_end, date_start, description, name, status, user_id) " +
                    "VALUES (#{id},#{createdData},#{endDate},#{startDate},#{description},#{name},#{status},#{userId})"
    )
    void persist(@NotNull final Project writeEntity) throws ConnectionLostException, SQLException;

    @Update(
            "UPDATE tm_morozov.app_project SET date_create = #{createdData}, date_end = #{endDate}, date_start = #{startDate}, " +
                    "name = #{name}, description = #{description}, status = #{status},user_id = #{userId}" +
                    "WHERE  id = #{id}"
    )
    void merge(@NotNull final Project updateEntity) throws ConnectionLostException, SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_project WHERE id=#{id}"
    )
    void remove(@NotNull final String id) throws ConnectionLostException, SQLException;

    @Delete(
            "DELETE FROM tm_morozov.app_project"
    )
    void removeAll() throws ConnectionLostException, SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_project WHERE user_id = #{userId}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<Project> findAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_project WHERE user_id = #{userId} AND id = #{id}"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String id) throws ConnectionLostException, SQLException;

    @Select(
            "SELECT * FROM tm_morozov.app_project WHERE user_id = #{userId} AND " +
                    "(name LIKE CONCAT('%', #{string}, '%') OR description LIKE CONCAT('%', #{string}, '%'))"
    )
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "createdData", column = "date_create"),
            @Result(property = "endDate", column = "date_end"),
            @Result(property = "startDate", column = "date_start"),
            @Result(property = "description", column = "description"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<Project> findProjectByStringInNameOrDescription(@Param(value = "userId") @NotNull final String userId,
                                                         @Param(value = "string")@NotNull final String string)
            throws ConnectionLostException, SQLException;
    @Delete(
            "DELETE FROM tm_morozov.app_project WHERE user_id = #{userId}"
    )
    void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;
}
