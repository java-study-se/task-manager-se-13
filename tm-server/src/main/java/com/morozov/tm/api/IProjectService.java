package com.morozov.tm.api;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    @NotNull
    List<Project> findAllProject() throws ConnectionLostException, SqlCustomException;

    @NotNull
    List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException, ConnectionLostException, SqlCustomException;
    @NotNull
    Project addProject(@NotNull String userId, @NotNull String projectName) throws StringEmptyException, ConnectionLostException, SqlCustomException;

    boolean removeProjectById(@NotNull String userId, @NotNull String id) throws StringEmptyException, ConnectionLostException, SqlCustomException;

    void updateProject(@NotNull String userId, @NotNull String id, @NotNull String projectName,
                       @NotNull String projectDescription, @NotNull String dataStart, @NotNull String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException, ProjectNotFoundException, ConnectionLostException, SqlCustomException;

    @NotNull
    List<Project> findProjectByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
            throws ConnectionLostException, SqlCustomException;

    void removeAllByUserId(@NotNull String userId) throws ConnectionLostException, SqlCustomException;

    void clearProjectList() throws ConnectionLostException, SqlCustomException;

    void loadProjectList(@Nullable final List<Project> loadProjectList) throws ConnectionLostException, SqlCustomException;
}
