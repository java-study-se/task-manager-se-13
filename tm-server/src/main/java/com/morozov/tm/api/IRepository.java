package com.morozov.tm.api;

        import com.morozov.tm.entity.AbstractEntity;
        import com.morozov.tm.exception.ConnectionLostException;
        import org.jetbrains.annotations.NotNull;
        import org.jetbrains.annotations.Nullable;

        import java.sql.SQLException;
        import java.util.List;

public interface IRepository<T extends AbstractEntity> {
    @NotNull
    List<T> findAll() throws ConnectionLostException, SQLException;
    @Nullable
    T findOne(@NotNull final String id) throws SQLException, ConnectionLostException;

    void persist(@NotNull final T writeEntity) throws ConnectionLostException, SQLException;

    void merge(@NotNull final T updateEntity) throws ConnectionLostException, SQLException;

    void remove(@NotNull final String id) throws ConnectionLostException, SQLException;

    void removeAll() throws ConnectionLostException, SQLException;
    void load(@NotNull final List<T> loadEntityList) throws ConnectionLostException, SQLException;
}
