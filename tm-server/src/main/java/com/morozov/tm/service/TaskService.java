package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.api.ITaskService;
import com.morozov.tm.entity.Task;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;


public final class TaskService implements ITaskService {
    @Nullable
    final private SqlSessionFactory sessionFactory;

    public TaskService(@Nullable final SqlSessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public @NotNull List<Task> findAllTask() throws ConnectionLostException, SqlCustomException {
        @NotNull List<Task> taskList;
        if (sessionFactory == null) throw new ConnectionLostException();
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository == null) throw new ConnectionLostException();
            taskList = taskRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByUserId(@NotNull final String userId)
            throws RepositoryEmptyException, ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        @NotNull List<Task> taskList;
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository == null) throw new ConnectionLostException();
            taskList = taskRepository.findAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        DataValidationUtil.checkEmptyRepository(taskList);
        return taskList;
    }

    @Override
    @NotNull
    public Task addTask(@NotNull final String userId, @NotNull final String taskName, @NotNull final String projectId)
            throws StringEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(taskName, projectId);
        if (sessionFactory == null) throw new ConnectionLostException();
        final @NotNull Task task;
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            task = new Task();
            task.setName(taskName);
            task.setIdProject(projectId);
            task.setUserId(userId);
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                taskRepository.persist(task);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return task;
    }

    @Override
    public boolean removeTaskById(@NotNull final String userId, @NotNull final String id)
            throws StringEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(id);
        if (sessionFactory == null) throw new ConnectionLostException();
        boolean isDeleted;
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            isDeleted = false;
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                if (taskRepository.findOneByUserId(userId, id) != null) {
                    taskRepository.remove(id);
                    session.commit();
                    isDeleted = true;
                }
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return isDeleted;
    }

    @Override
    public void updateTask(
            @NotNull final String userId, @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull final String dataStart, @NotNull final String dataEnd,
            @NotNull final String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException, TaskNotFoundException,
            ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                DataValidationUtil.checkEmptyRepository(taskRepository.findAll());
                DataValidationUtil.checkEmptyString(id, name, description, projectId);
                final Task task = taskRepository.findOne(id);
                if (task == null) throw new TaskNotFoundException();
                task.setId(id);
                task.setName(name);
                task.setDescription(description);
                final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
                if (updatedStartDate != null) {
                    task.setStatus(StatusEnum.PROGRESS);
                }
                task.setStartDate(updatedStartDate);
                final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
                if (updatedEndDate != null) {
                    task.setStatus(StatusEnum.READY);
                }
                task.setEndDate(updatedEndDate);
                task.setIdProject(projectId);
                task.setUserId(userId);
                taskRepository.merge(task);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws StringEmptyException, RepositoryEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(projectId);
        if (sessionFactory == null) throw new ConnectionLostException();
        @NotNull List<Task> resultTaskList;
        try (@Nullable final SqlSession session = sessionFactory.openSession();) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository == null) throw new ConnectionLostException();
            resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        DataValidationUtil.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                taskRepository.deleteAllTaskByProjectId(userId, projectId);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    public void clearTaskList() throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                taskRepository.removeAll();
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    @NotNull
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        @NotNull List<Task> taskListByProjectId;
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            if (taskRepository == null) throw new ConnectionLostException();
            if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
            taskListByProjectId = taskRepository.findTaskByStringInNameOrDescription(userId, string);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        return taskListByProjectId;
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                if (taskRepository == null) throw new ConnectionLostException();
                taskRepository.removeAllByUserId(userId);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    public void loadTaskList(@Nullable final List<Task> loadList) throws ConnectionLostException, SqlCustomException {
        if (loadList != null) {
            for (Task task : loadList) {
                if (sessionFactory == null) throw new ConnectionLostException();
                try (SqlSession session = sessionFactory.openSession()) {
                    if (session == null) throw new ConnectionLostException();
                    try {
                        @Nullable final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
                        if (taskRepository == null) throw new ConnectionLostException();
                        taskRepository.persist(task);
                        session.commit();
                    } catch (SQLException e) {
                        session.rollback();
                        e.printStackTrace();
                        throw new SqlCustomException(e);
                    }
                }
            }
        }
    }
}

