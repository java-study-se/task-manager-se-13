package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public final class ProjectService implements com.morozov.tm.api.IProjectService {

    final private SqlSessionFactory sessionFactory;

    public ProjectService(@Nullable final SqlSessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @NotNull
    public final List<Project> findAllProject() throws ConnectionLostException, SqlCustomException {
        @NotNull List<Project> projectList;
        if (sessionFactory == null) throw new ConnectionLostException();
        try(@Nullable final SqlSession session = sessionFactory.openSession();) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository == null) throw new ConnectionLostException();
            projectList = projectRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        return projectList;
    }

    @Override
    @NotNull
    public final List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException,
            ConnectionLostException, SqlCustomException {
        @NotNull List<Project> projectListByUserId;
        if (sessionFactory == null) throw new ConnectionLostException();
        try(@Nullable final SqlSession session = sessionFactory.openSession();) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository == null) throw new ConnectionLostException();
            projectListByUserId = projectRepository.findAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        DataValidationUtil.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }

    @NotNull
    @Override
    public final Project addProject(@NotNull final String userId, @NotNull final String projectName)
            throws StringEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(projectName);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        if (sessionFactory == null) throw new ConnectionLostException();
        try (@Nullable SqlSession session = sessionFactory.openSession()) {
            try {
                if (session == null) throw new ConnectionLostException();
                @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                if (projectRepository == null) throw new ConnectionLostException();
                projectRepository.persist(project);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return project;
    }

    @Override
    public final boolean removeProjectById(@NotNull final String userId, @NotNull String id) throws StringEmptyException,
            ConnectionLostException, SqlCustomException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (sessionFactory == null) throw new ConnectionLostException();
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                if (projectRepository == null) throw new ConnectionLostException();
                if (projectRepository.findOneByUserId(userId, id) != null) {
                    projectRepository.remove(id);
                    session.commit();
                    isDeleted = true;
                }
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return isDeleted;
    }

    @Override
    public final void updateProject(@NotNull final String userId, @NotNull final String id,
                                    @NotNull final String projectName, @NotNull final String projectDescription,
                                    @NotNull final String dataStart, @NotNull final String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException, ProjectNotFoundException,
            ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                if (projectRepository == null) throw new ConnectionLostException();
                DataValidationUtil.checkEmptyRepository(projectRepository.findAllByUserId(userId));
                DataValidationUtil.checkEmptyString(id, projectName, projectDescription);
                final Project project = projectRepository.findOne(id);
                if (project == null) throw new ProjectNotFoundException();
                project.setId(id);
                project.setName(projectName);
                project.setDescription(projectDescription);
                final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
                if (updatedStartDate != null) {
                    project.setStatus(StatusEnum.PROGRESS);
                }
                project.setStartDate(updatedStartDate);
                final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
                if (updatedEndDate != null) {
                    project.setStatus(StatusEnum.READY);
                }
                project.setEndDate(updatedEndDate);
                project.setUserId(userId);
                projectRepository.merge(project);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    @NotNull
    public final List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException, SqlCustomException {
        @NotNull List<Project> projectListByUserId;
        if (sessionFactory == null) throw new ConnectionLostException();
        try(@Nullable final SqlSession session = sessionFactory.openSession();) {
            if (session == null) throw new ConnectionLostException();
            @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            if (projectRepository == null) throw new ConnectionLostException();
            if (string.isEmpty()) return projectRepository.findAllByUserId(userId);
            projectListByUserId = projectRepository.findProjectByStringInNameOrDescription(userId, string);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SqlCustomException(e);
        }
        return projectListByUserId;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (@Nullable final SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                if (projectRepository == null) throw new ConnectionLostException();
                projectRepository.removeAllByUserId(userId);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    public final void clearProjectList() throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                if (projectRepository == null) throw new ConnectionLostException();
                projectRepository.removeAll();
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    public void loadProjectList(@Nullable List<Project> loadProjectList) throws ConnectionLostException, SqlCustomException {
        if (loadProjectList != null) {
            for (Project project : loadProjectList) {
                if (sessionFactory == null) throw new ConnectionLostException();
                try (SqlSession session = sessionFactory.openSession()) {
                    if (session == null) throw new ConnectionLostException();
                    try {
                        @Nullable final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
                        if (projectRepository == null) throw new ConnectionLostException();
                        projectRepository.persist(project);
                    } catch (SQLException e) {
                        session.rollback();
                        e.printStackTrace();
                        throw new SqlCustomException(e);
                    }
                }
            }
        }
    }
}
