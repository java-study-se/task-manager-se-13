package com.morozov.tm.service;

import com.morozov.tm.api.IDomainService;
import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.entity.Domain;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import org.jetbrains.annotations.NotNull;



public class DomainService implements IDomainService {
    protected IServiceLocator serviceLocator;

    public DomainService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void load(@NotNull final Domain domain) throws ConnectionLostException, SqlCustomException {
        serviceLocator.getProjectService().loadProjectList(domain.getProjectList());
        serviceLocator.getTaskService().loadTaskList(domain.getTaskList());
        serviceLocator.getUserService().loadUserList(domain.getUserList());
    }

    @Override
    public void export(@NotNull final Domain domain) throws ConnectionLostException, SqlCustomException {
        domain.setProjectList(serviceLocator.getProjectService().findAllProject());
        domain.setTaskList(serviceLocator.getTaskService().findAllTask());
        domain.setUserList(serviceLocator.getUserService().findAllUser());
    }
}
