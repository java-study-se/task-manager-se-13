package com.morozov.tm.service;

import com.morozov.tm.api.*;
import com.morozov.tm.util.EndpointPublishUtil;
import com.morozov.tm.util.SqlSessionFactoryUtil;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class Bootstrap implements IServiceLocator {
    @Nullable
    final private SqlSessionFactory sqlSessionFactory = SqlSessionFactoryUtil.getSqlSession();
    @NotNull
    final private IProjectService projectService = new ProjectService(sqlSessionFactory);
    @NotNull
    final private ITaskService taskService = new TaskService(sqlSessionFactory);
    @NotNull
    final private IUserService userService = new UserService(sqlSessionFactory);
    @NotNull
    final private IDomainService domainService = new DomainService(this);
    @NotNull
    final private ISessionService sessionService = new SessionService(sqlSessionFactory);

    public void init() {
        EndpointPublishUtil.publish(this);
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }
}

