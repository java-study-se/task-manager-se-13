package com.morozov.tm.service;

import com.morozov.tm.api.ISessionRepository;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.SqlCustomException;
import com.morozov.tm.util.SessionUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Date;

public class SessionService implements ISessionService {
    @Nullable
    final private SqlSessionFactory sessionFactory;

    public SessionService(@Nullable final SqlSessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Session getNewSession(User user) throws ConnectionLostException, SqlCustomException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setUserRoleEnum(user.getRole());
        session.setTimestamp(new Date());
        Session sessionWithSign = SessionUtil.sign(session);
        if(sessionFactory == null) throw new  ConnectionLostException();
        try (SqlSession sqlSession = sessionFactory.openSession()) {
            if (sqlSession == null) throw new ConnectionLostException();
            try {
                @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                if (sessionRepository == null) throw new ConnectionLostException();
                sessionRepository.persist(session);
                sqlSession.commit();
            } catch (SQLException e) {
                sqlSession.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return sessionWithSign;
    }

    @NotNull
    @Override
    public Session validate(@Nullable final Session session) throws AccessFirbidenException, CloneNotSupportedException,
            ConnectionLostException, SqlCustomException {
        if (session == null) throw new AccessFirbidenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessFirbidenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessFirbidenException();
        if (session.getTimestamp() == null || !SessionUtil.isSessionTimeLive(session.getTimestamp()))
            throw new AccessFirbidenException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessFirbidenException();
        @NotNull final String signatureOrigin = session.getSignature();
        @Nullable final String signatureTemp = SessionUtil.sign(temp).getSignature();
        final boolean check = signatureOrigin.equals(signatureTemp);
        if (!check) throw new AccessFirbidenException();
        if(sessionFactory == null) throw new  ConnectionLostException();
        try (SqlSession sqlSession = sessionFactory.openSession()) {
            if (sqlSession == null) throw new ConnectionLostException();
            try {
                @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                if (sessionRepository == null) throw new ConnectionLostException();
                if (sessionRepository.findOne(session.getId()) == null) throw new AccessFirbidenException();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return session;
    }

    @Override
    public void closeSession(@NotNull final Session session) throws ConnectionLostException, SqlCustomException {
        if(sessionFactory == null) throw new  ConnectionLostException();
        try (SqlSession sqlSession = sessionFactory.openSession()) {
            if (sqlSession == null) throw new ConnectionLostException();
            try {
                @Nullable final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
                if (sessionRepository == null) throw new ConnectionLostException();
                sessionRepository.remove(session.getId());
                sqlSession.commit();
            } catch (SQLException e) {
                sqlSession.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }
}
