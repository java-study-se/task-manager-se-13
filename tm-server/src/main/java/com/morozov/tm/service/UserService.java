package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.*;
import com.morozov.tm.util.DataValidationUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {
    @Nullable
    final private SqlSessionFactory sessionFactory;

    public UserService(@Nullable final SqlSessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @NotNull
    @Override
    public User loginUser(@NotNull final String login, @NotNull final String password)
            throws UserNotFoundException, StringEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(login, password);
        if (sessionFactory == null) throw new ConnectionLostException();
        @Nullable User user;
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable final IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                user = userRepository.findOneByLogin(login);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        if (user == null) throw new UserNotFoundException();
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash == null) throw new UserNotFoundException();
        if (!typePasswordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public String registryUser(@NotNull final String login, @NotNull final String password)
            throws UserExistException, StringEmptyException, ConnectionLostException, SqlCustomException {
        DataValidationUtil.checkEmptyString(login, password);
        if (sessionFactory == null) throw new ConnectionLostException();
        @NotNull final String userId;
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                @Nullable User oneByLogin = userRepository.findOneByLogin(login);
                if (oneByLogin != null) throw new UserExistException();
                @NotNull final User user = new User();
                user.setLogin(login);
                user.setRole(UserRoleEnum.USER);
                final String hashPassword = MD5HashUtil.getHash(password);
                if (hashPassword == null) throw new StringEmptyException();
                user.setPasswordHash(hashPassword);
                userRepository.persist(user);
                session.commit();
                userId = user.getId();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return userId;
    }


    @Override
    public void updateUserPassword(@NotNull final String id, @NotNull final String newPassword) throws StringEmptyException,
            UserNotFoundException, ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                @Nullable User user = userRepository.findOne(id);
                if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
                DataValidationUtil.checkEmptyString(newPassword);
                if (user == null) throw new UserNotFoundException();
                String hashNewPassword = MD5HashUtil.getHash(newPassword);
                if (hashNewPassword != null) {
                    user.setPasswordHash(hashNewPassword);
                }
                userRepository.merge(user);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    @Override
    public void updateUserProfile(@NotNull final String id, @NotNull final String newUserName, @NotNull final String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                @Nullable User user = userRepository.findOne(id);
                if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
                DataValidationUtil.checkEmptyString(newUserName, newUserPassword);
                if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
                if (user == null) throw new UserNotFoundException();
                user.setLogin(newUserName);
                String hashNewUserPassword = MD5HashUtil.getHash(newUserPassword);
                if (hashNewUserPassword != null) {
                    user.setPasswordHash(hashNewUserPassword);
                }
                userRepository.merge(user);
                session.commit();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
    }

    public void loadUserList(@Nullable final List<User> userList) throws ConnectionLostException, SqlCustomException {
        if (userList != null) {
            for (User user : userList) {
                if (sessionFactory == null) throw new ConnectionLostException();
                try (SqlSession session = sessionFactory.openSession()) {
                    if (session == null) throw new ConnectionLostException();
                    try {
                        @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                        if (userRepository == null) throw new ConnectionLostException();
                        userRepository.persist(user);
                        session.commit();
                    } catch (SQLException e) {
                        session.rollback();
                        e.printStackTrace();
                        throw new SqlCustomException(e);
                    }
                }
            }
        }
    }

    @Override
    public @NotNull List<User> findAllUser() throws ConnectionLostException, SqlCustomException {
        if (sessionFactory == null) throw new ConnectionLostException();
        @NotNull List<User> resultList;
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                resultList = userRepository.findAll();
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        return resultList;
    }

    @NotNull
    @Override
    public User findOneById(@NotNull String id) throws UserNotFoundException, ConnectionLostException, SqlCustomException {
        @Nullable User user;
        if (sessionFactory == null) throw new ConnectionLostException();
        try (SqlSession session = sessionFactory.openSession()) {
            if (session == null) throw new ConnectionLostException();
            try {
                @Nullable IUserRepository userRepository = session.getMapper(IUserRepository.class);
                if (userRepository == null) throw new ConnectionLostException();
                user = userRepository.findOne(id);
            } catch (SQLException e) {
                session.rollback();
                e.printStackTrace();
                throw new SqlCustomException(e);
            }
        }
        if (user == null) throw new UserNotFoundException();
        if (!user.getId().equals(id)) throw new UserNotFoundException();
        return user;
    }
}
