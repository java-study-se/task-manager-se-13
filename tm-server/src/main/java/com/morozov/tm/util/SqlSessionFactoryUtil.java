package com.morozov.tm.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.io.IOException;
import java.io.Reader;

public class SqlSessionFactoryUtil {
    @Nullable
    public static SqlSessionFactory getSqlSession(){
        @Nullable SqlSessionFactory sqlSessionFactory = null;
        Reader reader;
        @NotNull final String resource = "mybatis-config.xml";
        try {
            reader = Resources
                    .getResourceAsReader(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            System.out.println("Factory " + sqlSessionFactory.getClass().getSimpleName() + " OK");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sqlSessionFactory;
    }
}
